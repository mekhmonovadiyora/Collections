# Collections

- Web-app for personal collections managements 

## Features
- Docker for infrastructure
- Google Cloud Platform (Virtual machine) for server
- Godaddy for domain https://scirel.xyz
- Node version 19.7.0
- React for frontend
- Node js for backend
- postgresql for database
- TypeORM for orm
- Dadabase diagramm https://dbdiagram.io/d/64001624296d97641d84cc5a
- ChakraUI for ui library
- TailwindCSS for css framework
