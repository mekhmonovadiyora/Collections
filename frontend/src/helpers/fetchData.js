import axios from "axios";

const token = localStorage.getItem("token");

const instance = axios.create({
	baseURL: import.meta.env.VITE_BASE_URL,
	timeout: 10000,
	headers: {
		"X-Custom-Header": "foobar",
		Authorization: `Bearer ${token}`,
		"Content-Type": "application/json",
	},
});

const fetchData = ({ url, method, body = null, params = null }) => {
	return new Promise((resolve, reject) => {
		instance[method](url, body, params).then(resolve).catch(reject);
	});
};

export default fetchData;
