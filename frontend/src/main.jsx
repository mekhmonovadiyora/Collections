import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Login from "./pages/Login";
import Root from "./pages/Root";
import ChakraProvider from "./providers/ChakraProvider";
import "./App.css";
import Register from "./pages/Register";
import CollectionCreate from "./pages/CollectionCreate";
import CollectionForView from "./pages/CollectionForView";
import Profile from "./pages/Profile";
import ItemView from "./pages/ItemView";

const token = localStorage.getItem("token");

const router = createBrowserRouter([
	{
		path: "/",
		element: <Root />,
	},
	{
		path: "/login",
		element: <Login />,
	},
	{
		path: "/register",
		element: <Register />,
	},
	{
		path: "/collection/create",
		element: <CollectionCreate />,
	},
	{
		path: "/collection/:id",
		element: <CollectionForView />,
	},
	{
		path: token && "/profile",
		element: token && <Profile />,
	},
	{
		path: "/item/:id",
		element: <ItemView />,
	},
]);

ReactDOM.createRoot(document.getElementById("root")).render(
	<React.StrictMode>
		<ChakraProvider>
			<RouterProvider router={router} />
		</ChakraProvider>
	</React.StrictMode>
);
