import { Heading, VStack } from "@chakra-ui/react";
import LoginForm from "../components/LoginForm";

export default function Login() {
    return (
        <VStack className="w-full bg-sky-600 h-screen justify-center">
            <Heading>Login</Heading>
            <LoginForm/>
        </VStack>
    )
}
