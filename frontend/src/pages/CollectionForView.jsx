import { Heading, Text, VStack } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import ItemCreate from "../components/ItemCreate";
import Table from "../components/Table";
import fetchData from "../helpers/fetchData";
import Page from "../layouts/Page";

const token = localStorage.getItem("token");

export default function CollectionForView() {
	const { id } = useParams();
	const [collection, setCollection] = useState({});
	const [items, setItems] = useState([]);
	useEffect(() => {
		fetchData({ url: `collections/${id}`, method: "get" }).then((res) => {
			setCollection(res?.data);
		});
		fetchData({ url: `collections-item/${id}`, method: "get" }).then((res) => {
			setItems(res?.data);
		});
	}, []);

	return (
		<Page>
			<VStack>
				<Heading>{collection?.name}</Heading>
				<Text>{collection?.description}</Text>
			</VStack>
			{token && <ItemCreate isCreated={true} id={id} setItems={setItems} />}
			<Table items={items?.items} setItems={setItems} />
		</Page>
	);
}
