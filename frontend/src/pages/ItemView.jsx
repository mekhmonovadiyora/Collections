import {
	Button,
	Flex,
	Heading,
	HStack,
	Input,
	Text,
	useToast,
	VStack,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useParams } from "react-router-dom";
import fetchData from "../helpers/fetchData";
import Page from "../layouts/Page";

const token = localStorage.getItem("token");

export default function ItemView() {
	const { id } = useParams();
	const [itemData, setItemData] = useState();
	const [comments, setComments] = useState([]);
	const { register, handleSubmit } = useForm();
	const toast = useToast();

	setTimeout(() => {
		fetchData({ url: `item-comment/${id}`, method: "get" }).then((res) => {
			setComments(res?.data);
		});
	}, 5000);

	const onSubmit = (data) => {
		fetchData({
			url: `item-comment/${id}`,
			method: "post",
			body: data,
		})
			.then((res) => {
				toast({
					title: "Comment saved.",
					status: "success",
					duration: 3000,
					isClosable: true,
					position: "top-center",
				});
				fetchData({ url: `item-comment/${id}`, method: "get" }).then((res) => {
					setComments(res?.data);
				});
			})
			.catch((err) => {
				toast({
					title: "Comment not saved.",
					status: "error",
					duration: 3000,
					isClosable: true,
					position: "top-center",
				});
			});
	};

	useEffect(() => {
		fetchData({ url: `collections-item-one/${id}`, method: "get" }).then(
			(res) => {
				setItemData(res?.data);
			}
		);

		fetchData({ url: `item-comment/${id}`, method: "get" }).then((res) => {
			setComments(res?.data);
		});
	}, []);
	return (
		<Page>
			<VStack>
				<Heading>{itemData?.name}</Heading>
				<Flex className="flex-row flex-wrap">
					{itemData?.tags?.map((tag, index) => (
						<Text key={index} className="text-blue-600">
							{tag?.name}
						</Text>
					))}
				</Flex>
			</VStack>

			<VStack className="lg:w-1/3 w-full gap-6">
				<Heading size="md">Comments</Heading>
				{comments?.map((comment, index) => (
					<Flex key={index} className="w-full gap-4">
						<Text className="text-yellow-600">
							{comment?.createdBy?.username}
						</Text>
						<Text className="text-gray-600">{comment?.body}</Text>
					</Flex>
				))}

				{token && (
					<form onSubmit={handleSubmit(onSubmit)}>
						<HStack className="gap-4">
							<Input
								{...register("message", {
									required: true,
									minLength: { value: 1, message: "valid comment is required" },
								})}
								variant="flushed"
								placeholder="type..."
							/>
							<Button type="submit" variant="ghost">
								Send
							</Button>
						</HStack>
					</form>
				)}
			</VStack>
		</Page>
	);
}
