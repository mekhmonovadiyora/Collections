import { Heading, VStack } from "@chakra-ui/react";
import RegisterForm from "../components/RegisterForm";

export default function Register() {
	return (
		<VStack className="w-full bg-sky-600 h-screen justify-center">
			<Heading>Register</Heading>
			<RegisterForm />
		</VStack>
	);
}
