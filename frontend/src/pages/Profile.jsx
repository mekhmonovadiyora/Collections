import Page from "../layouts/Page";
import { VStack, Heading, Flex, Button } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import fetchData from "../helpers/fetchData";
import { useEffect, useState } from "react";
import Card from "../components/Card";

const token = localStorage.getItem("token");

export default function Profile() {
	const navigate = useNavigate();
	const [collections, setCollections] = useState([]);

	const onDelete = (id) => {
		fetchData({
			url: `collections/${id}`,
			method: "delete",
		})
			.then(() => {
				fetchData({ url: "collections", method: "get" }).then((res) => {
					setCollections(res?.data);
				});
			})
			.catch((err) => {});
	};

	useEffect(() => {
		fetchData({ url: "collections", method: "get" }).then((res) => {
			setCollections(res?.data);
		});
	}, []);
	return (
		<Page>
			<VStack className="w-full container md:gap-10 gap-6">
				<Flex className="md:flex-row flex-col w-full justify-between">
					<Heading>{name}</Heading>
					{token && (
						<Button size="lg" onClick={() => navigate("/collection/create")}>
							Add
						</Button>
					)}
				</Flex>
				<Flex className="md:flex-row flex-col flex-wrap w-full lg:gap-6 gap-3">
					{collections?.map((collection, index) => (
						<Card collection={collection} onDelete={onDelete} key={index} index={index} />
					))}
				</Flex>
			</VStack>
		</Page>
	);
}
