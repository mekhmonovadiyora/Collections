import {
	Button,
	Flex,
	FormLabel,
	Heading,
	Input,
	Select,
	Textarea,
	useToast,
	VStack,
} from "@chakra-ui/react";
import ImgUpload from "../components/ImgUpload";
import Page from "../layouts/Page";
import { useForm } from "react-hook-form";
import { useState } from "react";
import fetchData from "../helpers/fetchData";
import Table from "../components/Table";
import ItemCreate from "../components/ItemCreate";
import { useNavigate } from "react-router-dom";

const topics = ["books", "photos", "stamps", "coins", "cards", "other"];

export default function CollectionCreate() {
	const { register, handleSubmit } = useForm();
	const [imageFile, setImageFile] = useState(null);
	const [isCreated, setIsCreated] = useState(false);
	const [id, setId] = useState(null);
	const toast = useToast();
	const [items, setItems] = useState([]);
	const navigate = useNavigate();

	const onSubmit = (data) => {
		fetchData({
			url: "/collections",
			method: "post",
			body: data,
		})
			.then((res) => {
				setIsCreated(true);
				toast({
					title: "Collection saved.",
					status: "success",
					duration: 3000,
					isClosable: true,
					position: "top-center",
				});
				setId(res?.data?.id);

				navigate(`/collection/${res?.data?.id}`);

				id &&
					fetchData({ url: `/collections-item/${id}`, method: "get" }).then(
						(res) => {
							setItems(res?.items);
							console.log("res", res);
						}
					);
			})
			.catch((err) => {
				toast({
					title: err?.message || "Something went wrong.",
					status: "error",
					duration: 3000,
					isClosable: true,
					position: "top-center",
				});
			});
	};

	return (
		<Page>
			<Heading>Collection</Heading>
			<form
				id="collection"
				onSubmit={handleSubmit(onSubmit)}
				className="w-full flex flex-col lg:gap-8 gap-4"
			>
				<Flex className="lg:flex-row flex-col gap-6 w-full">
					<VStack align="left" className="w-full">
						<FormLabel>Name</FormLabel>
						<Input
							{...register("name", {
								required: true,
								minLength: { value: 3, message: "valid name is required" },
							})}
						/>
					</VStack>
					<VStack align="left" className="w-full">
						<FormLabel>Topic</FormLabel>
						<Select
							{...register("topic", {
								required: true,
								minLength: { value: 3, message: "valid name is required" },
							})}
						>
							{topics.map((topic, i) => {
								return (
									<option key={i} value={topic}>
										{topic}
									</option>
								);
							})}
						</Select>
					</VStack>
				</Flex>

				<Flex className="lg:flex-row flex-col gap-6 w-full">
					<VStack align="left" className="w-full">
						<FormLabel>Description</FormLabel>
						<Textarea
							{...register("description", {
								required: true,
								minLength: { value: 6, message: "valid name is required" },
							})}
							resize="none"
						/>
					</VStack>

					<VStack align="left" className="w-full">
						<FormLabel>Image</FormLabel>
						<Flex className="md:flex-row flex-col justify-between">
							<ImgUpload setImageFile={setImageFile} />
							<Button
								form="collection"
								type="submit"
								colorScheme="blue"
								className="justify-self-end"
							>
								Save
							</Button>
						</Flex>
					</VStack>
				</Flex>
			</form>
			{/* <ItemCreate id={id} setItems={setItems} isCreated={isCreated} />

			{isCreated && <Table items={items?.items} />} */}
		</Page>
	);
}
