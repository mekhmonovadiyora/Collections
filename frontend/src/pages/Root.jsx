import App from "../App.jsx";
import Page from "../layouts/Page.jsx";

export default function Root() {
	return (
		<Page>
			<App name="Collections" />
		</Page>
	);
}
