import { ChakraProvider } from "@chakra-ui/react";
import theme from "../theme/index";

export default function Chakra({ children }) {
	return <ChakraProvider theme={theme}>{children}</ChakraProvider>;
}
