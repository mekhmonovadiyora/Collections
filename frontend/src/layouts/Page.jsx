import { VStack } from "@chakra-ui/react";
import Navbar from "../components/Navbar";

export default function Page(props) {
	return (
		<VStack className="w-full md:gap-10 gap-6">
			<Navbar />
			<VStack className={"container lg:gap-8 gap-4 "+props.containerClass}>{props.children}</VStack>
		</VStack>
	);
}
