import { Box, HStack, Text, VStack } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import Delete from "../assets/icons/Delete";

const colors = [
	"red.100",
	"yellow.100",
	"blue.100",
	"green.100",
	"purple.100",
	"teal.100",
	"pink.100",
	"cyan.100",
	"orange.100",
];

const token = localStorage.getItem("token");

export default function Card({ collection, onDelete, index }) {
	const navigate = useNavigate();
	return (
		<VStack
			align="flex-start"
			className={`lg:p-6 p-3 rounded-2xl 2xl:w-[18.7%] xl:w-[23.5%] lg:w-[31.5%] md:w-[48.5%] w-full cursor-pointer `}
			backgroundColor={colors[index % colors.length]}
			onClick={() => navigate(`/collection/${collection?.id}`)}
		>
			<HStack className="justify-between w-full">
				<Text className="font-semibold text-lg">{collection?.name}</Text>
				{token && (
					<Box
						onClick={(e) => {
							e.stopPropagation();
							onDelete(collection?.id);
						}}
						className="cursor-pointer"
					>
						<Delete />
					</Box>
				)}
			</HStack>

			<HStack>
				<Text className="font-semibold">Topic: </Text>
				<Text>{collection?.topic}</Text>
			</HStack>
		</VStack>
	);
}
