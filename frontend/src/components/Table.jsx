import {
	TableContainer,
	Table as ChakraTable,
	Thead,
	Tbody,
	Tr,
	Th,
	Td,
	Text,
	Flex,
	useToast,
} from "@chakra-ui/react";
import { useParams } from "react-router-dom";
import Delete2 from "../assets/icons/Delete2";
import Edit from "../assets/icons/Edit";
import fetchData from "../helpers/fetchData";

const token = localStorage.getItem("token");

export default function Table({ items, setItems }) {
	const { id } = useParams();
	const toast = useToast();

	const onDelete = (itemId) => {
		fetchData({ url: `/collections-item/${itemId}`, method: "delete" })
			.then((res) => {
				fetchData({
					url: `/collections-item/${id}`,
					method: "get",
				}).then((res) => {
					setItems(res?.data);
					toast({
						title: "Item deleted.",
						status: "success",
						duration: 3000,
						isClosable: true,
						position: "top-center",
					});
				});
			})
			.catch((err) => {
				toast({
					title: err?.message || "Something went wrong.",
					status: "error",
					duration: 3000,
					isClosable: true,
					position: "top-center",
				});
			});
	};
	return (
		<TableContainer className="w-full">
			<ChakraTable variant="simple">
				<Thead>
					<Tr>
						<Th className="!text-lg !text-black">Item name</Th>
						<Th className="!text-lg !text-black">Tags</Th>
						{token && <Th className="!text-lg !text-black">Actions</Th>}
					</Tr>
				</Thead>
				<Tbody>
					{items?.map((item, index) => (
						<Tr key={index}>
							<Td>{item?.name}</Td>
							<Td>
								<Flex className="md:flex-row flex-col gap-2">
									{item?.tags?.map((tag, index) => (
										<Text key={index} className="text-blue-600">
											{tag?.name}
										</Text>
									))}
								</Flex>
							</Td>
							{token && (
								<Td>
									<Flex className="md:flex-row flex-col gap-4">
										<Delete2 onClick={() => onDelete(item?.id)} />
										<Edit id={item?.id} />
									</Flex>
								</Td>
							)}
						</Tr>
					))}
				</Tbody>
			</ChakraTable>
		</TableContainer>
	);
}
