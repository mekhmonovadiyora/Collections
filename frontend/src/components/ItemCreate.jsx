import {
	Button,
	FormLabel,
	Input,
	Modal,
	ModalBody,
	ModalCloseButton,
	ModalContent,
	ModalHeader,
	ModalOverlay,
	useDisclosure,
	useToast,
	VStack,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { components } from "react-select";
import Creatable from "react-select/creatable";
import fetchData from "../helpers/fetchData";

const Menu = (props) => {
	const optionSelectedLength = props.getValue().length || 0;
	return (
		<components.Menu {...props}>
			{optionSelectedLength < 5 ? (
				props.children
			) : (
				<div style={{ margin: 15 }}>Max limit achieved</div>
			)}
		</components.Menu>
	);
};

export default function ItemCreate({ isCreated, id, setItems }) {
	const { isOpen, onOpen, onClose } = useDisclosure();
	const { register, handleSubmit, reset } = useForm();
	const [selectedValue, setSelectedValue] = useState([]);
	const [tags, setTags] = useState([]);
	const toast = useToast();

	const isValidNewOption = (inputValue, selectValue) =>
		inputValue.length > 0 && selectValue.length < 5;

	const onSubmit = (data) => {
		fetchData({
			url: `/collections-item/${id}`,
			method: "post",
			body: { ...data, tags: selectedValue },
		})
			.then((res) => {
				onClose();
				toast({
					title: "Collection saved.",
					status: "success",
					duration: 3000,
					isClosable: true,
					position: "top-center",
				});
				fetchData({ url: `/collections-item/${id}`, method: "get" }).then(
					(res) => setItems(res?.data)
				);
				reset();
			})
			.catch((err) => {
				toast({
					title: err?.message || "Something went wrong.",
					status: "error",
					duration: 3000,
					isClosable: true,
					position: "top-center",
				});
			});
	};

	useEffect(() => {
		fetchData({ url: "/tag", method: "get" }).then((res) => {
			let options = [];

			res?.data?.forEach((tag) => {
				options.push({ value: tag?.name, label: tag?.name });
			});
			setTags(options);
		});
	}, []);

	return (
		<>
			<Button
				onClick={isCreated ? onOpen : null}
				disabled={isCreated}
				colorScheme="yellow"
				opacity={isCreated ? 1 : 0.5}
				variant="ghost"
				className="w-full"
			>
				Add item
			</Button>

			<Modal isOpen={isOpen} onClose={onClose}>
				<ModalOverlay />
				<ModalContent>
					<ModalHeader>Item</ModalHeader>
					<ModalCloseButton />
					<ModalBody>
						<form id="item" onSubmit={handleSubmit(onSubmit)}>
							<VStack className="gap-4">
								<VStack align="left" className="w-full gap-0">
									<FormLabel>Name</FormLabel>
									<Input
										{...register("name", {
											required: true,
											minLength: {
												value: 3,
												message: "valid name is required",
											},
										})}
									/>
								</VStack>
								<VStack align="left" className="w-full gap-0">
									<FormLabel>Tags</FormLabel>
									<Creatable
										className="w-full"
										components={{ Menu }}
										isMulti
										isValidNewOption={isValidNewOption}
										options={tags}
										onChange={(e) => {
											let tagNames = [];
											e.forEach((item) => {
												tagNames.push(item.value);
											});
											setSelectedValue(tagNames);
										}}
									/>
								</VStack>
								<Button
									form="item"
									colorScheme="blue"
									type="submit"
									className="self-end"
								>
									Save
								</Button>
							</VStack>
						</form>
					</ModalBody>
				</ModalContent>
			</Modal>
		</>
	);
}
