import { Button, Input, VStack, Text, Link } from "@chakra-ui/react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import fetchData from "../helpers/fetchData";

export default function LoginForm() {
	const [username, setUsername] = useState();
	const [password, setPassword] = useState();
	const [isError, setError] = useState(false);
	const [errorMessage, setErrorMessage] = useState("");
	const navigate = useNavigate();

	const handleCLick = () => {
		if (username?.length < 3 || password?.length < 6) {
			setError(true);
		} else {
			fetchData({
				url: "/auth/login",
				method: "post",
				body: { username, password },
			})
				.then((res) => {
					setUsername("");
					setPassword("");
					setError(false);
					console.log(res);
					localStorage.setItem("username", username);
					localStorage.setItem("token", res?.data?.token);
					navigate("/");
				})
				.catch((err) => {
					setUsername("");
					setPassword("");
					setErrorMessage(err?.response?.data?.message);
				});
		}
	};

	return (
		<VStack className="md:p-10 p-4 w-[500px] backdrop-blur-md backdrop-filter gap-4">
			<VStack className="w-full">
				<Input
					placeholder="username"
					className="bg-white rounded-3xl border"
					focusBorderColor="gray.300"
					_placeholder={{ opacity: 1, color: "gray.700" }}
					size="lg"
					value={username || ""}
					onChange={(e) => {
						setUsername(e.target.value);
						setErrorMessage("");
					}}
				/>
				{isError && username.length < 3 && (
					<Text className="text-red-600 font-medium ml-2 text-base">
						valid username is required
					</Text>
				)}
			</VStack>

			<VStack className="w-full">
				<Input
					placeholder="password"
					className="bg-white rounded-3xl border"
					focusBorderColor="gray.300"
					_placeholder={{ opacity: 1, color: "gray.700" }}
					size="lg"
					value={password || ""}
					onChange={(e) => {
						setPassword(e.target.value);
						setErrorMessage("");
					}}
				/>
				{isError && password.length < 6 && (
					<Text className="text-red-600 font-medium ml-2 text-base">
						valid password is required
					</Text>
				)}
			</VStack>

			<VStack className="w-full">
				{errorMessage && (
					<Text className="text-red-600 font-medium ml-2 text-base">
						{errorMessage}
					</Text>
				)}
				<Button className="w-full" onClick={handleCLick}>
					Login
				</Button>
				<Text className="font-semibold">
					Don't have an account?
					<Link className="!text-white" href="/register">
						{" "}
						Register
					</Link>
				</Text>
			</VStack>
		</VStack>
	);
}
