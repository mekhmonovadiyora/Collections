import { Center, Heading, HStack } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

export default function Navbar() {
	const navigate = useNavigate();
	const isLogged = localStorage.getItem("token") ? true : false;
	const username = localStorage.getItem("username");
	return (
		<Center className="bg-sky-600 w-full shadow-lg shadow-blue-500/50">
			<HStack className="w-full py-6 container justify-between">
				<Heading
					className="!text-2xl cursor-pointer"
					onClick={() => navigate("/")}
				>
					Home
				</Heading>
				<HStack className="md:gap-6">
					<Heading
						className="!text-xl cursor-pointer"
						onClick={() => navigate("/profile")}
					>
						{username && (
							<Center className="bg-white h-10 w-10 rounded-full">
								{username?.[0]}
							</Center>
						)}
					</Heading>

					<Heading
						className="!text-xl cursor-pointer"
						onClick={() => {
							navigate("/login");
							if (isLogged) {
								localStorage.removeItem("username");
								localStorage.removeItem("token");
							}
						}}
					>
						{isLogged ? "Sign out" : "Login"}
					</Heading>
				</HStack>
			</HStack>
		</Center>
	);
}
