import { Button, Input, Link, Text, VStack } from "@chakra-ui/react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import fetchData from "../helpers/fetchData";

export default function RegisterForm() {
	const [username, setUsername] = useState();
	const [password, setPassword] = useState();
	const [confirmPassword, setConfirmPassword] = useState();
	const [isError, setError] = useState(false);
	const [errorMessage, setErrorMessage] = useState("");
	const navigate = useNavigate();

	const handleCLick = () => {
		if (
			username?.length < 3 ||
			password?.length < 6 ||
			password !== confirmPassword
		) {
			setError(true);
		} else {
			fetchData({
				url: "/auth/register",
				method: "post",
				body: { username, password, confirmPassword },
			})
				.then((res) => {
					setUsername("");
					setPassword("");
					setConfirmPassword("");
					setError(false);
					navigate("/");
					localStorage.setItem("username", username);
					localStorage.setItem("token", res?.data?.token);
				})
				.catch((err) => {
					setUsername("");
					setPassword("");
					setConfirmPassword("");
					setErrorMessage(err?.response?.data?.message);
				});
		}
	};
	return (
		<VStack className="md:p-10 p-4 w-[500px] backdrop-blur-md backdrop-filter gap-4">
			<VStack className="w-full">
				<Input
					placeholder="username"
					className="bg-white rounded-3xl border"
					size="lg"
					focusBorderColor="gray.300"
					_placeholder={{ opacity: 1, color: "gray.700" }}
					value={username || ""}
					onChange={(e) => {
						setUsername(e.target.value);
						setErrorMessage("");
					}}
				/>
				{isError && username.length < 3 && (
					<Text className="text-red-600 font-medium ml-2 text-base">
						valid username is required
					</Text>
				)}
			</VStack>

			<VStack className="w-full">
				<Input
					placeholder="password"
					className="bg-white rounded-3xl border"
					size="lg"
					focusBorderColor="gray.300"
					_placeholder={{ opacity: 1, color: "gray.700" }}
					value={password || ""}
					onChange={(e) => {
						setPassword(e.target.value);
						setErrorMessage("");
					}}
				/>
				{isError && password.length < 6 && (
					<Text className="text-red-600 font-medium ml-2 text-base">
						valid password is required
					</Text>
				)}
			</VStack>

			<VStack className="w-full">
				<Input
					placeholder="confirm password"
					className="bg-white rounded-3xl border"
					size="lg"
					focusBorderColor="gray.300"
					_placeholder={{ opacity: 1, color: "gray.700" }}
					value={confirmPassword || ""}
					onChange={(e) => {
						setConfirmPassword(e.target.value);
						setErrorMessage("");
					}}
				/>
				{isError && confirmPassword !== password && (
					<Text className="text-red-600 font-medium ml-2 text-base">
						passwords do not match
					</Text>
				)}
			</VStack>

			<VStack className="w-full">
				{errorMessage && (
					<Text className="text-red-600 font-medium ml-2 text-base">
						{errorMessage}
					</Text>
				)}
				<Button className="w-full" onClick={handleCLick}>
					Register
				</Button>
				<Text className="font-semibold">
					Already registered?
					<Link className="!text-white" href="/login">
						{" "}
						Login
					</Link>
				</Text>
			</VStack>
		</VStack>
	);
}
