import { Flex, Heading, HStack, Text, VStack } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Card from "./components/Card";
import fetchData from "./helpers/fetchData";

const token = localStorage.getItem("token");

function App() {
	const [collections, setCollections] = useState([]);
	const [latestItems, setLatestItems] = useState([]);
	const navigate = useNavigate();

	useEffect(() => {
		fetchData({
			url: "/collections?sortField=totalItems&sortOrder=DESC",
			method: "get",
		}).then((res) => {
			setCollections(res?.data);
		});
		fetchData({
			url: "/collections-item",
			method: "get",
		}).then((res) => {
			setLatestItems(res?.data?.items);
		});
	}, []);

	return (
		<Flex className="lg:flex-row flex-col-reverse w-full">
			<VStack className="lg:w-[60%] w-full gap-6">
				<Heading size="lg">Biggest collections</Heading>
				<Flex className="md:flex-row flex-col flex-wrap w-full lg:gap-6 gap-3">
					{collections?.map((collection, index) => (
						<Card collection={collection} key={index} index={index} />
					))}
				</Flex>
			</VStack>
			<VStack className="lg:w-[40%] w-full gap-6">
				<Heading size="lg">Latest items</Heading>
				<Flex className="md:flex-row flex-col flex-wrap w-full lg:gap-6 gap-3">
					{latestItems?.map((latestItem, index) => (
						<HStack
							onClick={() => navigate(`item/${latestItem?.id}`)}
							key={index}
							className="w-full bg-blue-300 p-4 rounded-2xl cursor-pointer"
						>
							<Text className="font-semibold">{latestItem.name}</Text>
						</HStack>
					))}
				</Flex>
			</VStack>
		</Flex>
	);
}

export default App;
