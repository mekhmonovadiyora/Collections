import "reflect-metadata";
import { DataSource } from "typeorm";
import { User } from "./entity/User";
import { Collection } from "./entity/Collection";
import { CollectionItem } from "./entity/CollectionItem";
import { RoleMigration } from "./migrations/RoleMigration";
import { Tag } from "./entity/Tag";
import { Comment } from "./entity/Comment";

export const AppDataSource = new DataSource({
	type: "postgres",
	host: process.env.POSTGRES_HOST,
	port: 5432,
	username: "postgres",
	password: "postgres",
	database: "collections",
	synchronize: true,
	logging: false,
	entities: [User, Collection, CollectionItem, Tag, Comment],
	migrations: [RoleMigration],
	subscribers: [],
});
