import { MigrationInterface, QueryRunner } from "typeorm";

export class TotalItemsTrigger implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`
			CREATE OR REPLACE FUNCTION update_total_items()
			RETURNS TRIGGER AS $$
			BEGIN
				UPDATE "collection" SET total_items = total_items + 1 WHERE id = NEW.collection_id;
				RETURN NEW;
			END;
			$$ language 'plpgsql';

			CREATE TRIGGER update_total_items
			AFTER INSERT ON "collection_item"
			FOR EACH ROW
			EXECUTE PROCEDURE update_total_items();
			
			`
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
