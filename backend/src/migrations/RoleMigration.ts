import { MigrationInterface, QueryRunner } from "typeorm";

export class RoleMigration implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`INSERT INTO "role" (id, name, translation) VALUES (1, 'user', '{"ru":"пользователь", "en":"user", "uz":"foydalanuvchi"}'), (2, 'admin', '{"ru":"администратор", "en":"admin", "uz":"admin"}')`
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
