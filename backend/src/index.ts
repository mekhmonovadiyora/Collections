import * as express from "express";
import * as bodyParser from "body-parser";
import { Request, Response, NextFunction } from "express";
import { AppDataSource } from "./data-source";
import { Routes } from "./routes";
import * as cors from "cors";
import { config } from "dotenv";

config({
	path: "./.env",
});

AppDataSource.initialize()
	.then(async () => {
		// create express app
		const app = express();

		app.use(bodyParser.json());

		app.use(
			cors({
				origin: "*",
			})
		);
		app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
			console.error(err.stack);
			return res.status(500).send("Something broke!");
		});

		// register express routes from defined application routes
		Routes.forEach((route) => {
			(app as any)[route.method](
				route.route,
				...route.middlewares,
				(req: Request, res: Response, next: Function) => {
					try {
						new (route.controller as any)()[route.action](req, res, next);
					} catch (error) {
						console.log(error);
						return res.json({
							error: error.message,
						});
					}
				}
			);
		});

		// setup express app here
		// ...

		// start express server
		const port = process.env.PORT ?? 3000;
		app.listen(port);

		console.log(
			`Express server has started on port ${port}. Open http://localhost:${port}/ to see results`
		);
	})
	.catch((error) => console.log(error));
