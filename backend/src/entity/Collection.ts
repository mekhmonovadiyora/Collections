import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	OneToMany,
	CreateDateColumn,
	UpdateDateColumn,
	DeleteDateColumn,
	EventSubscriber,
} from "typeorm";
import { CollectionItem } from "./CollectionItem";

@Entity()
export class Collection {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	name: string;

	@Column()
	description: string;

	@Column({ nullable: true })
	image?: string;

	@OneToMany(() => CollectionItem, (item) => item.collection)
	items: CollectionItem[];

	@Column({
		type: "enum",
		enum: ["books", "photos", "stamps", "coins", "cards", "other"],
	})
	topic: string;

	@Column({ name: "total_items", default: 0 })
	totalItems: number;

	@CreateDateColumn()
	created!: Date;

	@UpdateDateColumn()
	updated!: Date;

	@DeleteDateColumn()
	deletedAt?: Date;
}
