import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
} from "typeorm";

@Entity()
export class User {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	username: string;

	@Column()
	password: string;

	@Column({
		type: "enum",
		enum: ["user", "admin"],
		default: "user",
	})
	role: "user" | "admin";
}
