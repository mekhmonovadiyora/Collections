import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	ManyToOne,
	JoinTable,
	UpdateDateColumn,
	ManyToMany,
	DeleteDateColumn,
	CreateDateColumn,
	AfterInsert,
	JoinColumn,
} from "typeorm";
import { Collection } from "./Collection";
import { Tag } from "./Tag";
import { User } from "./User";

@Entity()
export class CollectionItem {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	name: string;

	@ManyToOne(() => Collection, (collection) => collection.items, {
		onDelete: "CASCADE",
		nullable: false,
	})
	@JoinColumn({ name: "collection_id" })
	collection: Collection;

	@ManyToOne(() => User, {
		onDelete: "CASCADE",
		nullable: false,
	})
	@JoinColumn({ name: "created_by" })
	createdBy: User;

	@ManyToMany(() => Tag)
	@JoinTable()
	tags: Tag[];

	@CreateDateColumn()
	created!: Date;

	@UpdateDateColumn()
	updated!: Date;

	@DeleteDateColumn()
	deletedAt?: Date;

	@AfterInsert()
	async updateCollection() {
		this.collection.totalItems++;
	}
}
