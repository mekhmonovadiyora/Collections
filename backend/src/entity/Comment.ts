import {
	Column,
	CreateDateColumn,
	Entity,
	JoinColumn,
	ManyToOne,
	PrimaryGeneratedColumn,
} from "typeorm";
import { CollectionItem } from "./CollectionItem";
import { User } from "./User";

@Entity()
export class Comment {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	body: string;

	@ManyToOne(() => CollectionItem, {
		onDelete: "CASCADE",
		nullable: false,
	})
	@JoinColumn({ name: "item_id" })
	collectionItem: CollectionItem;

	@ManyToOne(() => User, {
		onDelete: "CASCADE",
		nullable: false,
	})
	@JoinColumn({ name: "created_by" })
	createdBy: User;

	@CreateDateColumn()
	created!: Date;
}
