import { NextFunction, Request, Response } from "express";
import { AppDataSource } from "../data-source";
import { Collection } from "../entity/Collection";
import { CollectionItem } from "../entity/CollectionItem";
import { Tag } from "../entity/Tag";
import { User } from "../entity/User";

export class ItemController {
	private itemRepository = AppDataSource.getRepository(CollectionItem);
	private collectionRepository = AppDataSource.getRepository(Collection);
	private tagRepository = AppDataSource.getRepository(Tag);

	async allCollectionItems(req: Request, res: Response, next: NextFunction) {
		const id = parseInt(req.params.id);

		const collection = this.itemRepository.findOne({ where: { id } });
		if (!collection) {
			return res.status(404).json({ message: "Collection not found" });
		}

		const items = await this.itemRepository.find({
			where: { collection: { id } },
			relations: ["tags"],
		});

		return res.json({
			items: items,
		});
	}

	async all(req: Request, res: Response, next: NextFunction) {
		const items = await this.itemRepository.find({
			relations: ["tags"],
			order: { created: "DESC" },
		});

		return res.json({
			items: items,
		});
	}

	async one(req: Request, res: Response, next: NextFunction) {
		const id = parseInt(req.params.id);

		const item = await this.itemRepository.findOne({
			where: { id },
			relations: ["collection", "tags"],
		});
		if (!item) {
			return res.status(404).json(item);
		}

		return res.json(item);
	}

	async save(req: Request, res: Response, next: NextFunction) {
		const id = parseInt(req.params.id);

		const { name, tags } = req.body;

		let jwt = JSON.parse(req.headers.jwt.toString());

		const collection = await this.collectionRepository.findOne({
			where: { id: id },
		});

		if (!collection) {
			return res.status(400).json({
				message: "collection not found",
			});
		}

		let tagsArray = [];

		for (let i = 0; i < tags.length; i++) {
			let tag = new Tag();
			tag.name = tags[i];

			tagsArray.push(tag);
		}

		const tagUpsertResult = await this.tagRepository.upsert(tagsArray, [
			"name",
		]);

		let createdBy = new User();
		createdBy.id = jwt.id;

		let item = new CollectionItem();
		item.name = name;
		item.collection = collection;
		item.tags = tagsArray;
		item.createdBy = createdBy;

		const result = this.itemRepository.save(item);

		// upsert item tags

		return res.json(result);
	}

	async remove(req: Request, res: Response, next: NextFunction) {
		const id = parseInt(req.params.id);
		console.log(id);

		const item = await this.itemRepository.findOne({ where: { id } });
		if (!item) {
			return res.status(404).json({ message: "Item not found" });
		}

		await this.itemRepository
			.createQueryBuilder()
			.softDelete()
			.where("id=:id", { id })
			.execute();

		return res.json({ message: "Item deleted" });
	}
}
