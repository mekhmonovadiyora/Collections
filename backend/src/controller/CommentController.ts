import { NextFunction, Request, Response } from "express";
import { AppDataSource } from "../data-source";
import { CollectionItem } from "../entity/CollectionItem";
import { Comment } from "../entity/Comment";
import { User } from "../entity/User";

export class CommentController {
	private commentRepo = AppDataSource.getRepository(Comment);
	private itemRepository = AppDataSource.getRepository(CollectionItem);

	async allItemComments(req: Request, res: Response, next: NextFunction) {
		const id = parseInt(req.params.id);

		const item = await this.itemRepository.findOne({ where: { id } });
		if (!item) {
			return res.status(404).json({
				message: "Item not found",
			});
		}

		const comments = await this.commentRepo.find({
			where: {
				collectionItem: {
					id: id,
				},
			},
			relations: ["createdBy"],
			order: {
				created: "ASC",
			},
		});

		return res.json(comments);
	}

	async save(req: Request, res: Response, next: NextFunction) {
		const id = parseInt(req.params.id);

		const { message } = req.body;

		let jwt = JSON.parse(req.headers.jwt.toString());

		const item = await this.itemRepository.findOne({ where: { id } });
		if (!item) {
			return res.status(404).json({
				message: "Item not found",
			});
		}

		let createdBy = new User();
		createdBy.id = jwt.id;

		let comment = new Comment();

		comment.body = message;
		comment.collectionItem = item;
		comment.createdBy = createdBy;

		const result = await this.commentRepo.save(comment);

		return res.json(result);
	}
}
