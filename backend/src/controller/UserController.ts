import { AppDataSource } from "../data-source";
import { NextFunction, Request, Response } from "express";
import { User } from "../entity/User";
import { hash, compare } from "bcrypt";
import { sign } from "jsonwebtoken";

export class UserController {
	private userRepository = AppDataSource.getRepository(User);

	async all(request: Request, response: Response, next: NextFunction) {
		return response.json(await this.userRepository.find());
	}

	async one(request: Request, response: Response, next: NextFunction) {
		const id = parseInt(request.params.id);

		const user = await this.userRepository.findOne({
			where: { id },
		});

		if (!user) {
			return "unregistered user";
		}
		return user;
	}

	async save(request: Request, response: Response, next: NextFunction) {
		const { firstName, lastName, age } = request.body;

		const user = Object.assign(new User(), {
			firstName,
			lastName,
			age,
		});

		return this.userRepository.save(user);
	}

	async remove(request: Request, response: Response, next: NextFunction) {
		const id = parseInt(request.params.id);

		let userToRemove = await this.userRepository.findOneBy({ id });

		if (!userToRemove) {
			return "this user not exist";
		}

		await this.userRepository.remove(userToRemove);

		return "user has been removed";
	}

	async login(request: Request, response: Response, next: NextFunction) {
		const { username, password } = request.body;

		const user = await this.userRepository.findOne({
			where: { username },
		});

		if (!user) {
			return response
				.status(401)
				.json({ message: "invalid username or password" });
		}

		const isPasswordValid = await compare(password, user.password);

		if (!isPasswordValid) {
			return response
				.status(401)
				.json({ message: "invalid username or password" });
		}

		const jwtToken = sign(
			{
				id: user.id,
				role: user.role,
			},
			"secret",
			{
				expiresIn: "72h",
			}
		);

		return response.json({
			token: jwtToken,
		});
	}

	async register(request: Request, response: Response, next: NextFunction) {
		const { username, password, confirmPassword } = request.body;

		if (password !== confirmPassword) {
			return response.status(400).json({ message: "passwords do not match" });
		}

		const user = await this.userRepository.findOne({
			where: { username },
		});

		if (user) {
			return response.status(400).json({ message: "username already exists" });
		}

		const hashPassword = await hash(password, 10);

		const newUser = Object.assign(new User(), {
			username: username,
			password: hashPassword,
		});

		let res = await this.userRepository.save(newUser);

		console.log(res);

		const jwtToken = sign(
			{
				id: newUser.id,
				role: res.role,
			},
			"secret",
			{
				expiresIn: "24h",
			}
		);

		return response.json({
			token: jwtToken,
		});
	}

	async getMyself(request: Request, response: Response, next: NextFunction) {
		const jwt = JSON.parse(request.headers.jwt as string);

		const user = await this.userRepository.findOne({
			where: { id: jwt.id },
			select: ["role", "username"],
		});

		if (!user) {
			return response.json({ message: "no auth" });
		}
		return response.json(user);
	}
}
