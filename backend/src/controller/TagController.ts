import { NextFunction, Request, Response } from "express";
import { AppDataSource } from "../data-source";
import { Tag } from "../entity/Tag";

export class TagController {
	private tagRepository = AppDataSource.getRepository(Tag);

	async all(req: Request, res: Response, next: NextFunction) {
		const tags = await this.tagRepository.find();

		return res.json(tags);
    }
    
    
}
