import { NextFunction, Request, Response } from "express";
import { AppDataSource } from "../data-source";
import { Collection } from "../entity/Collection";

export class CollectionController {
	private collectionRepository = AppDataSource.getRepository(Collection);

	async all(request: Request, response: Response, next: NextFunction) {
		let sortField = request.query.sortField || "created";
		let sortOrder = request.query.sortOrder || "ASC";

		return response.json(
			await this.collectionRepository.find({
				order: {
					[sortField.toString()]: sortOrder,
				},
			})
		);
	}

	async one(request: Request, response: Response, next: NextFunction) {
		const id = parseInt(request.params.id);

		const collection = await this.collectionRepository.findOne({
			where: { id },
		});

		if (!collection) {
			return response.status(404).json("not found");
		}
		return response.status(200).json(collection);
	}

	async create(request: Request, response: Response, next: NextFunction) {
		const { name, description, topic, image } = request.body;

		const collection = Object.assign(new Collection(), {
			name,
			description,
			topic,
			image,
		});

		const res = await this.collectionRepository.save(collection);

		return response.status(200).json({ id: res.id });
	}

	async delete(request: Request, response: Response, next: NextFunction) {
		const id = parseInt(request.params.id);

		let collection = this.collectionRepository.findOneBy({ id });

		if (!collection) {
			return response.status(404).json({ message: "not found" });
		}

		const res = await this.collectionRepository
			.createQueryBuilder()
			.softDelete()
			.where("id=:id", { id })
			.execute();

		return response.json(res);
	}
}
