import { CollectionController } from "./controller/CollectionController";
import { CommentController } from "./controller/CommentController";
import { ItemController } from "./controller/ItemController";
import { TagController } from "./controller/TagController";
import { UserController } from "./controller/UserController";
import Auth from "./middleware/Auth";

export const Routes = [
	{
		method: "get",
		route: "/users",
		controller: UserController,
		action: "all",
		middlewares: [Auth],
	},
	{
		method: "get",
		route: "/users/:id",
		controller: UserController,
		action: "one",
		middlewares: [Auth],
	},
	{
		method: "post",
		route: "/users",
		controller: UserController,
		action: "save",
		middlewares: [Auth],
	},
	{
		method: "delete",
		route: "/users/:id",
		controller: UserController,
		action: "remove",
		middlewares: [Auth],
	},
	{
		method: "get",
		route: "/user",
		controller: UserController,
		action: "getMyself",
		middlewares: [Auth],
	},
	{
		method: "post",
		route: "/auth/login",
		controller: UserController,
		action: "login",
		middlewares: [],
	},
	{
		method: "post",
		route: "/auth/register",
		controller: UserController,
		action: "register",
		middlewares: [],
	},
	{
		method: "get",
		route: "/collections",
		controller: CollectionController,
		action: "all",
		middlewares: [],
	},
	{
		method: "post",
		route: "/collections",
		controller: CollectionController,
		action: "create",
		middlewares: [Auth],
	},
	{
		method: "get",
		route: "/collections/:id",
		controller: CollectionController,
		action: "one",
		middlewares: [],
	},
	{
		method: "delete",
		route: "/collections/:id",
		controller: CollectionController,
		action: "delete",
		middlewares: [Auth],
	},

	{
		method: "get",
		route: "/collections-item/:id",
		controller: ItemController,
		action: "allCollectionItems",
		middlewares: [],
	},
	{
		method: "get",
		route: "/collections-item",
		controller: ItemController,
		action: "all",
		middlewares: [],
	},
	{
		method: "post",
		route: "/collections-item/:id",
		controller: ItemController,
		action: "save",
		middlewares: [Auth],
	},
	{
		method: "delete",
		route: "/collections-item/:id",
		controller: ItemController,
		action: "remove",
		middlewares: [Auth],
	},
	{
		method: "get",
		route: "/collections-item-one/:id",
		controller: ItemController,
		action: "one",
		middlewares: [],
	},
	{
		method: "get",
		route: "/tag",
		controller: TagController,
		action: "all",
		middlewares: [],
	},
	{
		method: "get",
		route: "/item-comment/:id",
		controller: CommentController,
		action: "allItemComments",
		middlewares: [],
	},
	{
		method: "post",
		route: "/item-comment/:id",
		controller: CommentController,
		action: "save",
		middlewares: [Auth],
	},
];
